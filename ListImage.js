import React, { Component } from 'react';
import { StyleSheet, View, Button, Image, Modal, TouchableOpacity } from 'react-native';

export default class App extends Component {
    state = {
        images: '',
        isShowModal: false
    }
    showModal(visible) {
        this.setState({ isShowModal: visible })
    }
    showImage(image) {
        this.setState({ images: image })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.row]}>
                    <View style={[styles.column]}>
                        <View style={[styles.pictureContainer]}>
                            <Modal
                                transparent={true}
                                visible={this.state.isShowModal}
                            >
                                <View style={[styles.row]}>
                                    <View style={[styles.modal]}>
                                        <Button title="X" onPress={() => {
                                            this.showModal(!this.state.isShowModal);
                                        }}></Button>
                                        <Image source={{ uri: this.state.images }}
                                            style={styles.imgModal} resizeMode="stretch"></Image>
                                    </View>
                                </View>
                            </Modal>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[styles.column]}>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.pictureContainer]}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.showModal(true)
                                    this.showImage('https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260')
                                }}
                            >
                                <Image source={{ uri: 'https://images.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' }}
                                    style={styles.img}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    column: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
    },
    text: {
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: 25
    },
    pictureContainer: {
        width: 150,
        height: 150,
        marginBottom: 50,
        marginLeft: 15
    },
    img: {
        borderRadius: 10,
        width: '100%', height: '100%',
        alignItems: 'center'

    },
    modal: {
        marginTop: 30,
        width: '80%',
        height: '80%',
        backgroundColor: 'white',
    },
    imgModal: {
        flex: 1,
    }
})
