import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Alert, TouchableHighlight, TextInput, Image, Modal } from 'react-native';

export default class App extends Component {
  state = {
    user: '',
    password: '',
    isShowModal: false
  }
  showModal(visible) {
    this.setState({ isShowModal: visible })
  }
  showImage(image) {
    this.setState({ images: image })
  }
  onValueChange = (field, value) => {
    this.setState({ [field]: value })
  }
  onLoggedIn() {
    const { Username } = this.state;
    const { Password } = this.state;
    if (Username === 'admin' && Password === 'eiei') {
      this.showModal(true)
      this.showImage('https://amp.businessinsider.com/images/5654150584307663008b4ed8-750-563.jpg')
    } else {
      alert('Username or password is incorrect!!!')
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          transparent={true}
          visible={this.state.isShowModal}
        >
          <Button title="X" onPress={() => {
            this.showModal(!this.state.isShowModal);
          }}></Button>
          <View style={styles.container}>
            <View style={[styles.modal]}>
              <Image source={{ uri: this.state.images }} borderRadius={50}
                style={styles.image}></Image>
              <Text style={[styles.text]}>{this.state.Username} - {this.state.Password}</Text>
              <Text style={[styles.text]}>Welcome to React Native!</Text>
            </View>
          </View>
        </Modal>
        <View><Image
          style={styles.image} borderRadius={50}
          source={{ uri: 'https://amp.businessinsider.com/images/5654150584307663008b4ed8-750-563.jpg' }}
        />
        </View>
        <Text>{this.state.Username} - {this.state.Password}</Text>
        <TextInput style={styles.textinput} borderRadius={10}
          onChangeText={value => this.onValueChange('Username', value)}
          placeholder='Username'
        />
        <TextInput style={styles.textinput} borderRadius={10}
          onChangeText={value => this.onValueChange('Password', value)}
          placeholder='Password'
        />
        <View style={styles.textinput} borderRadius={10}>
          <Button title="SUBMIT" onPress={this.onLoggedIn.bind(this)} />
        </View>
        <TouchableHighlight onPress={() => { Alert.alert('KILL HIM') }}>
          <Text style={styles.welcome}>Welcome to React Native!</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textinput: {
    width: '80%',
    borderWidth: 1,
  },
  modal: {
    marginTop: 30,
    width: '80%',
    height: '80%',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    textAlignVertical: 'center',
    textAlign: 'center',
    fontSize: 25
  },
  image: {
    width: 250,
    height: 240
  }
});
